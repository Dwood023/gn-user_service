const express = require("express")
const body_parser = require("body-parser")
var db = require("./db")

var app = express()

app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended: false }));

app.get("/ping", function(req, res) {
    res.send("pong")
})

app.post("/auth", async function(req, res) {

    var user = {
        username: req.body.username,
        password: req.body.password
    }

    try {
        var status = await db.has_user(user) ? 200 : 403;
        res.status(status).send()
    }
    catch(err) {
        res.status(400).send()
    }
})

app.listen(3000, () => console.log("Server started on port 3000!"))

// Tests can use express app from require statement
module.exports = app;