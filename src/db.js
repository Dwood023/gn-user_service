r = require("rethinkdb");

const db_options = {
    host: "localhost",
    port: 28015,
    db: "test"
}

var connection;

module.exports.connect = async function () {
    return r.connect(db_options)
        .then( (result) => connection = result)
}

module.exports.new_user = async function(user) {
    r.db("test").table("users")
        .insert(user)
        .run(connection)
}

// DB read is async, therefore this must return a promise
// "Promises" to eventually return whether DB contains user
module.exports.has_user = async function(user) {

    var results = await r.db("test").table("users") 
        .filter(user)
        .run(connection)
        .then( (cursor) => {return cursor.toArray()})

    // TODO -- Should many users with the same username?
    return results.length >= 1;
}
