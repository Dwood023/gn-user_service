var chai = require("chai");
var should = chai.should();
var expect = chai.expect;

var db = require("../src/db");

describe("Database connectivity", function () {

    it("Connects without error", function () {
        expect(db.connect).to.not.throw();
    });

})

describe("New user", function() {

    const real_user = { username: "real_user", password: "real_pass" };
    var good_call = () => db.new_user(real_user);

    it("Inserts arbitrary data", function() {
        expect(good_call).to.not.throw();
    });

})

describe("Has user", function () {

    const real_user = { username: "real_user", password: "real_pass" };
    const fake_user = { username: "fake_user", password: "fake_pass" };

    it("Returns true for existing user", function () {
        return db.has_user(real_user)
                .then((result) => result.should.be.true)
    });

    it("Returns false for non-existent user", function() {
        return db.has_user(fake_user)
                .then((result) => result.should.be.false)
    })
})