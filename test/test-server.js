var chai = require("chai")
    , should = chai.should()
    , chai_http = require("chai-http");

chai.use(chai_http);

var server = require("../src/server");

describe("GET /ping", function() {

    it("Basic ping test", function() {
        return chai.request(server).get("/ping")
            .then((res) => res.text.should.equal("pong"))
    });

})

describe("POST /auth", function() {

    var route = () => chai.request(server).post("/auth")

    const good_creds = { username: "real_user", password: "real_pass" };
    const bad_creds = { username: "fake_user", password: "fake_pass" };
    const invalid_data = { user: "wrong", pass: "fields" };

    it("Status 200 for successful login", function() {
        return route()
            .send(good_creds)
            .then((res) => res.should.have.status(200))
    })

    it("Status 403 for failed login", function() {
        return route()
            .send(bad_creds)
            // NOTE -- bad responses are returned as promised errors, so catch() instead of then()
            .catch((res) => res.should.have.status(403))
    })

    it("Status 400 for invalid data format", function() {
        return route()
            .send(invalid_data)
            .catch((res) => res.should.have.status(400))
    })
})